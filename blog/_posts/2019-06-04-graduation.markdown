---
layout: post
title:  "Graduation thoughts"
date:   2019-06-04 17:17:40 -0400
categories: graduation modernity history university
---

A few days ago, I graduated from university.
It was one of those odd sorts of days where little *actually* happens, but it feels like this huge, exhausting ordeal to sit through.
Just about the only thing I actually did, was dress up, line up, sit down, and walk up and across a stage and then drink up with friends after a bunch of photographs.
Hardly a day of difficult labour, yet it practically felt like one.
It's funny how days that are *supposed* to be filled with meaning are usually just sort of odd and tiring, save one or two moments where the reality of the situation reveals itself to you. 

The whole process of graduation itself feels weirdly archaic. 
Most obviously, there's the costumes.
It seems just about every single university has a different one, but at McGill anyhow, you wear a sort of odd hood that's not even attached to your robe.
My friends and I, when we lined up to graduate, were informed that we were in fact all wearing the hood wrong, showing just how familiar we were with the damn thing.
It's hard to imagine that not so long ago, did people *have* to wear outfits like this most of the time during their education.
One of those quaint little aspects of history that's been lost except in the margins.

Then there's the actual ceremony.
You're told to follow a very precise pattern, along with several hundred other people.
This pattern involves getting multiple pictures taken, a random officiant you don't know mispronouncing your name to a huge crowd, and a bop on the head with a hat.
Then you're graduated.
It's not really clear *why* one has to get the head-bop or why you have to stand at a certain angle.
It's kind of nice, really.
You learn what you have to do, then you do it.

There's very few of these kinds of ceremonies anymore. 
If you were Catholic, a hundred years ago you'd probably spend each Sunday morning taking the Eurachrist, but today that sort of thing is scarce and hard to come by.
We've sort of agreed that today, what's important is an individualism born out of capitalism rather than any sense of community and this is reflected in our new approaches to ceremony.
Ceremonies work for a community, they provide a sense of meaning and purpose and they allow for people to be sure of the role they play (in at least the ceremony).
Ceremonies make little sense to an individual; any ritual behaviour that someone rigourously follows that they themselves made up, could probably meet the diagnostic criterion for OCD, anyhow.
Even religious ceremonies are slowly fading away, the pomp and circumstance of a Catholic mass is replaced more and more by the more individualistic Evangelical "worship".
Rather than a mass in a language no one but the priest understands filled with specific and arbitrary moments, Evangelical churches eschew anything like that, in favour of a casual, personal connection to God.

Graduation, then, represents one of those few unique rituals left in someone's life. 
Rather than a thousand daily moments imbued with superstition, you have a handful, graduation, weddings, funerals. 
Maybe a few more or less depending on your current intensity of secularism, but never countless little rituals

Either way, I'm getting a little distracted.
But, that sort of connection to the past makes the graduation(an ostensibly entirely future-oriented affair, y'know, moving on to the next stage of life) seem oddly present.
The present too, is present, in a rather vulgar and insulting way. 
My own graduation featured a brief video that was played for the whole crowd, of various alumni talking about how connecting in this "global network"(i.e., the group of relative privilege that McGill graduates represent) empowered their careers or whatever neoliberal bullshit.
I could talk about what I had originally planned on writing, that the video represented a vulgar intrusion of capitalism into one of the few places left free of it---the ceremony, but what really struck me was not so much the tastelessness but something else. 
What was odd about these alumni is that they were all between the ages of about 24 and 26. 
The fact that someone who was 24 was so nostalgic for university that they joined a fucking *alumni* organisation, kind of concerned me. 

But a few weeks after convocation, and a month or two after convocation, it began to make sense to me.
Not so much actually going out an joining an alumni organisation before the age of 65, but more that sort of stark contrast between pre- and post-graduation.
It's a death of sorts.
From the ages of about 5 to about 22, depending on your nationality and preference for "gap years", many people are continually in education.
There's a nice cycle to education, each September you start a new year or grade, and each April or June you finish one. 
Every year is quite similar to the one before it, in at least the very basic form, you'll have a new professor or teacher, you'll learn some content, you'll do some work, you'll get some grades.
But each passing year builds on the previous, there's a clear progression to it, that can provide a sense of direction that you wouldn't otherwise have. 
Even if in one sense you're repeating yourself, you're still moving foreward. 
I could draw a diagram but what I'm envisaging is something akin to a rollercoaster with several loop-de-loops, each year's a loop, something similar to the previous one, yet somehow ahead.

University graduation, maybe in the past high-school graduation but today, university graduation represents a clear break of this roller-coaster.
Suddenly, the years don't simply repeat themselves with a few changes towards an overall goal.
This facade of a progression breaks down, suddenly the progression is much, *much* more active.
Rather than simply being pulled along to the next semester, now you have to *push* yourselves along.
It's like going along a highway for what feels like an eternity, then suddenly reaching the city and realising that you forgot why you were driving there in the first place.
