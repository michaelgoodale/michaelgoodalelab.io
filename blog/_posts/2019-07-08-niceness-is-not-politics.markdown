---
layout: post
title:  "Niceness is not politics"
date:   2019-06-12 22:25:40 -0400
categories: kindness nice politics leftism  
---

Everyone, barring the most socially disturbed, wants to be a good person.
You want to know that people trust you, that you're not upsetting other people for no good reason, that you're making the morally right choice.
In general, this is probably a good thing. 
It's good that people want to be good.
It's what lets people do favours for one another, or let's them say nice compliments or, even let's them make real sacrifices for one another.
I could scarcely imagine a society where everyone was as ruggedly self-interested as Randian might hope we'd be.
It would descend very quickly into a kind of lower-case a anarchy where nothing was sacred and everyone was holding a knife to your back.

One thing that's sort of taking for granted nowdays(and probably previously, but I wasn't alive then so I couldn't say), is that niceness is a part of being a good person.
Niceness can mean being polite and holding your tongue when you really want to say something cruel and vindictive.
It can mean taking into account everyone's different needs into account when you're hosting an event.
There's about a million things that niceness can mean, but generally it means not upsetting people's feelings.

This is a good thing, everyone's already fucking miserable under post-modernity, let's not make it any worse.
The problem arises though, when two important things conflict, a political programme, and Niceness.
Niceness is a great thing for the personal, it's important in interpersonal relationships and in just generally being kind.
The problem with politics though, is that it's not about the means *per se*, but about the ends.

We *need* to get things done.
The oceans are nearly boiling vats of vinegar at this point.
The US has literal concentration camps.
Plenty of Canadian Indigenious communities don't have drinking water.
I could go on, everyone knows the list at this point, I don't need to repeat it.
The thing is, these aren't abstract talking points.
They're real material issues and they're getting worse.

It's possible that on some level the reason for these issues is a lack of "niceness" on the part of the rich and powerful.
Clearly it's not a nice thing to do, to pour lead into a river to save a buck.
But the thing is, it doesn't fucking matter if it's nice or not. 
What matters is it's killing people, and if it's not killing people, it's making their lives *absolutely* wretched and miserable.
Not miserable in the way that it hurts when someone snubs you by not inviting you to their birthday party.
It's miserable in a fundmentally cruel and brutal way, which makes social niceity not even relevant.

There's an idea that only if the rich and powerful were nice, and kind, and caring, that things would be different, but I guaran-fucking-tee it wouldn't be.
I'm sure the rich and powerful are often kind to their friends and family, to their social acquantiences, hell maybe even some of them are polite to the "help".
But they still do awful shit, because it makes them money and as long as they can keep it out of their conscience, they're fine.

The real issue, however, is not the liberal fantasy of a "nice" elite. 
It's the leftist fantasy of a "nice" political programme.
Everyone knows that the elites are not nice, insofar as when they act as elites. 
Maybe if we're nice and we're kind in building a nice and kind society things can be better.

It's a really seductively nice idea. 
Maybe I *can* have the omelette and not break any eggs.
But the thing is, the forces you're up against don't give a flying fuck about niceness.
What matters is not that no one's feelings are ever hurt. 
Feelings get hurt, it's inevitable, it's part of life, get over it.
What matters is that everyone has a shot at a decent life where they aren't dying of heat-stroke under an intensifying sun, or trapped in a cage, or working some miserable job for less and less money, always stressed about being able to pay rent.

If we want a truly just and equitable society, it's possible that we might have to be mean to some people.
Some of those people might be elites, I think most people are fine with the idea of Rex Tillerson crying a little bit because his billions got taxxed.
The harder pill to swallow is however that some of those people will be people also fighting for a just society.
Some people are just not cut-out for some aspects of political programmes.
That's fine, pretty much everyone has something vital that they can be doing for a cause they believe it.
But we cannot let people's desire to be a revolutionary trump the goals of a revolution.

The goal is to change the world, not to let people *feel* like they are changing the world.
